//
//  NextViewController.swift
//  MyFirstProject
//
//  Created by Artyom on 17.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import Foundation
import UIKit

class NextViewController: UIViewController {
    
    var name: String!
    
    @IBOutlet weak var titleLable: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        titleLable.text = name
    }
}
