//
//  ViewController.swift
//  MyFirstProject
//
//  Created by Artyom on 10.10.2020.
//  Copyright © 2020 Artyom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    private let showSeque = "showScreen2"
    private let actorName = "Daniel Day-Lewis"
    
    @IBOutlet weak var oneButton: UIButton!
    
    override func viewDidDisappear(_ animated: Bool) {
        oneButton.tintColor = UIColor.brown
    }
    
    @IBAction func didTabOneButton(_ sender: Any) {
        performSegue(withIdentifier: showSeque, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? NextViewController {
            dest.name = actorName
        }
    }
}
